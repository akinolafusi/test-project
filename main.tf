provider "aws" {
  access_key = "your_access_key"
  secret_key = "your_secret_key"
  region     = "us-west-2"
}

resource "aws_instance" "example" {
  ami             = "ami-0c55b159cbfafe1f0"  # Replace with your desired AMI ID
  instance_type   = "t2.micro"
  key_name        = "your_key_pair_name"
  security_groups = ["your_security_group_name"]

  tags = {
    Name = "example-instance"
  }
}